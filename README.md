#Bitbucket Migrate Tool
###Ortaya Çıkış Amacı
Elimizde bulunan, eski sürüm (4.x) bir Bitbucket sunucusunu, en yeni sürümüne (6.0), ara bir sürüme yükseltme yapmaksızın, güncelleme işlemini tamamlamak. Aslında bu işlemi, Atlassian firmasının kendi yazdığı java tabanlı uygulama ile de yapabilirsiniz. Benim amacım, temiz bir Database kurulumu yapmak, önceki kurulumlardan gelen hataları bertaraf etmek ve en öncemlisi, bu iş için yeni bir yol bularak alternatif üretmek.

###Nasıl çalışır?
Çok basit bir çalışma prensibine dayanıyoruz. RestAPI. Temel olarak, kaynak Bitbucket sunucusundan okuduğumuz verileri, gerektiği şekilde düzenleyerek yenisinde çalıştırıyoruz.

###Neler yapabilir?
* Projeler için;
    * Kullanıcı izinleri
    * Grup izinleri
* Repositoryler için;
    * Kullanıcı izinleri
    * Grup izinleri
    * Branch izinleri
    * Pull-Requests ayarları
    * Hook ayarları
* İstediğiniz hookları, webhook olarak dönüştürebilir

###Konfigürasyon Örneği
**İpucu:** Eğer bir projenin altındaki tüm repositoryleri taşımak isterseniz, "<repository_name>" yazan alana, tırnak işareti içinde "*" koymanız yeterli olacaktır.
```json
{
  "from": {
    "host": "<bitbucket_hostname_or_ip>",
    "credentials": ["<bitbucket_username>","<bitbucket_password>"]
  },
  "to": {
    "host": "<bitbucket_hostname_or_ip>",
    "credentials": ["<bitbucket_username>","<bitbucket_password>"]
  },
  "items": {
    "<project_name>": [
      "<repository_name>"
    ]
  },
  "hooks": [
    "<hook_key>"
  ],
  "convert_to_webhooks": {
    "<hook_key>": {
      "url_key": "<url_key_for_getting_from_hook_key>",
      "events": [
        "repo:refs_changed"
      ]
    }
  }
}
```

###Nasıl çalıştırılır?
Aşağıdaki komutu çalıştırarak scripti başlatabilirsiniz. Daha sonrasında "/tmp/bitbucket_migrate_manager.log" dosyasından ilerlemeleri izleyebilirsiniz.
```bash
python bitbucket_migrate.py --config-file bitbucket_migrate.json 2> /dev/null
```