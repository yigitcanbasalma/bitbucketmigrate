#!/usr/bin/python
# -*- coding: utf-8 -*-

from json import loads as json_loader
from json import dumps as json_dumper
from sys import exit as exit_script
from time import sleep

import requests
import argparse
import logging

logging.basicConfig(filename="/tmp/bitbucket_migrate_manager.log", filemode="w", format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger("BitbucketMigrateManager")


class BitBucket(object):
    def __init__(self, config_file):
        config = self.__get_config_from_json(config_file=config_file)
        logger.info("Config got successfully.")
        self.getter_host = config["from"]["host"]
        self.getter_credentials = tuple(config["from"]["credentials"])
        self.setter_host = config["to"]["host"]
        self.setter_credentials = tuple(config["to"]["credentials"])
        self.migrate_items = config["items"]
        self.hooks = config["hooks"]
        self.convert_to_webhooks = config["convert_to_webhooks"]
        self.api_req_parameters = [
            "limit=250"
        ]
        self.permission_types = [
            "users",
            "groups"
        ]
        self.permission_type_convert = {
            "users": "user",
            "groups": "group"
        }

    # Main function

    def start_migration(self):
        # Find project info for project that defined in config file
        for project_key, project_name, is_public in self.__get_all_projects():
            # Create projects
            self.__create_project(project_name=project_name, project_key=project_key, is_public=is_public)
            # Find permissions defined in "self.permission_types"
            for perm_type in self.permission_types:
                # Get permission owner name and permission type
                for related_name, permission in self.__get_project_permissions(project_name=project_key, permission_type=perm_type):
                    # Define permission for project to owner
                    self.__set_project_permission(project_name=project_key, permission_type=perm_type, related_name=related_name, permission_name=permission)
            # Find repos belonging to the project
            for repo_name, clone_link in self.__get_all_repos_of_project(project_name=project_key):
                # Clone repo into new bitbucket server
                self.__import_external_repo(project_name=project_key, repo_name=repo_name, clone_url=clone_link)
                # Find permissions defined in "self.permission_types"
                for perm_type in self.permission_types:
                    # Get permission owner name and permission type
                    for related_name, permission in self.__get_repo_permissions(project_name=project_key, repo_name=repo_name, permission_type=perm_type):
                        # Define permission for project to owner
                        self.__set_repo_permission(project_name=project_key, repo_name=repo_name, permission_type=perm_type, related_name=related_name, permission_name=permission)
                # Find branch restriction rules
                for restriction_payload in self.__get_branch_restrictions(project_name=project_key, repo_name=repo_name):
                    # If restriction found, create in the new bitbucket server
                    self.__set_branch_restriction(project_name=project_key, repo_name=repo_name, payload=restriction_payload)
                # Create hooks if is set
                for hook_key in self.hooks:
                    self.__create_hook(project_name=project_key, repo_name=repo_name, hook_key=hook_key)
                # Convert some hooks to webhook if is set
                for hook_key in self.convert_to_webhooks:
                    webhook_events = self.convert_to_webhooks[hook_key]["events"]
                    webhook_url_identifier = self.convert_to_webhooks[hook_key]["url_key"]
                    self.__convert_to_webhook(project_name=project_key, repo_name=repo_name, hook_key=hook_key, hook_events=webhook_events, url_identifier=webhook_url_identifier)
                # Migrate pull-requests settings
                self.__set_pull_requests_config(project_name=project_key, repo_name=repo_name)

    # Helper functions

    @staticmethod
    def __get_config_from_json(config_file):
        logger.info("Getting config from '{0}'.".format(config_file))
        try:
            return json_loader(open(config_file, "r").read())
        except IOError:
            logger.error("Config file does not found!!!")
        except ValueError:
            logger.error("Config file type does not proper or file contains errors.")
        except Exception as e:
            logger.error("Got an exception when reading config file. Exception: '{0}'.".format(str(e)))

    def requester(self, url, headers=None, data=None, auth=None, r_type="text", method="GET", timeout=600, response_headers=False, verify=False):
        avail_methods = {
            "GET": requests.get,
            "POST": requests.post,
            "PUT": requests.put,
            "DELETE": requests.delete
        }
        if headers is None:
            headers = {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0 (Bitbucket Migration Manager)",
                "Content-Type": "application/json"
            }
        if method in ["POST"]:
            headers["Origin"] = str(self.setter_host)
        data = data if data is not None else dict()
        r = avail_methods[method](url, headers=headers, verify=verify, data=data, auth=auth, timeout=timeout)
        r_headers = ["{0}: {1}".format(k, v) for k, v in r.headers.iteritems()] if response_headers else []
        if r_type == "json":
            return r.status_code, r.json(), r_headers
        return r.status_code, r.text, r_headers

    # Getter functions

    def __get_all_projects(self):
        api_url = self.getter_host + "/rest/api/latest/projects?{0}".format("&".join(self.api_req_parameters))
        logger.info("Getting all project from '{0}'.".format(api_url))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                return [(i["key"], i["name"], i["public"]) for i in response["values"] if i["name"] in self.migrate_items.keys()]
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            exit_script(-1)
        except Exception as e:
            logger.error("Got an exception when getting projects. Exception: '{0}'.".format(str(e)))
            exit_script(-1)

    def __get_project_permissions(self, project_name, permission_type):
        api_url = self.getter_host + "/rest/api/latest/projects/{0}/permissions/{1}?{2}".format(project_name, permission_type, "&".join(self.api_req_parameters))
        logger.info("Getting project '{0}' permissions belonging to the '{1}' named project.".format(permission_type, project_name))
        try:
            related_name_key = self.permission_type_convert[permission_type]
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                logger.info("{0} permission were found.".format(len(response["values"])))
                return [(i[related_name_key]["name"], i["permission"]) for i in response["values"]]
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            exit_script(-1)
        except Exception as e:
            logger.error("Got an exception when getting project permissions. Exception: '{0}'.".format(str(e)))
            exit_script(-1)

    def __get_all_repos_of_project(self, project_name):
        api_url = self.getter_host + "/rest/api/latest/projects/{0}/repos?{1}".format(project_name, "&".join(self.api_req_parameters))
        logger.info("Getting all repos belonging to the '{0}' project.".format(project_name))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                logger.info("{0} repos were found.".format(len(response["values"])))
                repo_clone_links = list()
                for repo in response["values"]:
                    project_identifier = repo["project"]["name"]
                    if repo["name"] in self.migrate_items[project_identifier] or "*" in self.migrate_items[project_identifier]:
                        for info in repo["links"]["clone"]:
                            if info["name"] == "http":
                                repo_clone_links.append((repo["name"], info["href"]))
                return repo_clone_links
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            exit_script(-1)
        except Exception as e:
            logger.error("Got an exception when getting repos of the project. Exception: '{0}'.".format(str(e)))
            exit_script(-1)

    def __get_repo_permissions(self, project_name, repo_name, permission_type):
        api_url = self.getter_host + "/rest/api/latest/projects/{0}/repos/{1}/permissions/{2}?{3}".format(project_name, repo_name, permission_type, "&".join(self.api_req_parameters))
        logger.info("Getting repo '{0}' permissions belonging to the '{1}/{2}' named object.".format(permission_type, project_name, repo_name))
        try:
            related_name_key = self.permission_type_convert[permission_type]
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                logger.info("{0} permission were found.".format(len(response["values"])))
                return [(i[related_name_key]["name"], i["permission"]) for i in response["values"]]
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            exit_script(-1)
        except Exception as e:
            logger.error("Got an exception when getting project permissions. Exception: '{0}'.".format(str(e)))
            exit_script(-1)

    def __get_branch_restrictions(self, project_name, repo_name):
        api_url = self.getter_host + "/rest/branch-permissions/latest/projects/{0}/repos/{1}/restrictions?{2}".format(project_name, repo_name, "&".join(self.api_req_parameters))
        logger.info("Getting branch restrictions for '{0}/{1}' named object.".format(project_name, repo_name))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                branch_restrictions = list()
                for rest in response["values"]:
                    if rest["matcher"]["type"]["id"] == "BRANCH":
                        branch_restrictions.append({
                            "id": rest["id"],
                            "type": rest["type"],
                            "matcher": rest["matcher"],
                            "users": [i["name"] for i in rest["users"]] if len(rest["users"]) > 0 else list(),
                            "groups": rest["groups"]
                        })
                logger.info("{0} restrictions were found.".format(len(branch_restrictions)))
                return branch_restrictions
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            exit_script(-1)
        except Exception as e:
            logger.error("Got an exception when getting branch restrictions. Exception: '{0}'.".format(str(e)))
            exit_script(-1)

    def __get_hook_details(self, project_name, repo_name, hook_key):
        api_url = self.getter_host + "/rest/api/latest/projects/{0}/repos/{1}/settings/hooks/{2}".format(project_name, repo_name, hook_key)
        logger.info("Getting hook info for '{0}' from '{1}/{2}' named object.".format(hook_key, project_name, repo_name))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                return response["details"]["name"], response["enabled"], response["configured"]
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when getting hook info. Exception: '{0}'.".format(str(e)))

    def __get_hook_settings(self, project_name, repo_name, hook_key):
        api_url = self.getter_host + "/rest/api/latest/projects/{0}/repos/{1}/settings/hooks/{2}/settings".format(project_name, repo_name, hook_key)
        logger.info("Getting hook settings for '{0}' from '{1}/{2}' named object.".format(hook_key, project_name, repo_name))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                return response
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when getting hook settings. Exception: '{0}'.".format(str(e)))

    def __get_pull_requests_config(self, project_name, repo_name):
        api_url = self.getter_host + "/rest/api/latest/projects/{0}/repos/{1}/settings/pull-requests".format(project_name, repo_name)
        logger.info("Getting pull-requests settings.")
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.getter_credentials, r_type="json")
            if status_code == 200:
                return response
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when getting pull-requests settings. Exception: '{0}'.".format(str(e)))

    # Setter functions

    def __create_project(self, project_name, project_key, is_public=False):
        api_url = self.setter_host + "/rest/api/latest/projects"
        api_payload = json_dumper({
            "key": project_key,
            "name": project_name,
            "public": is_public
        })
        logger.info("Creating '{0}' named project.".format(project_name))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.setter_credentials, method="POST", data=api_payload)
            if status_code == 201:
                logger.info("Project created.")
                return
            elif status_code == 409:
                logger.info("Project name already exists. Skipping.")
                return
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            exit_script(-1)
        except Exception as e:
            logger.error("Got an exception when creating project. Exception: '{0}'.".format(str(e)))
            exit_script(-1)

    def __set_project_permission(self, project_name, permission_type, related_name, permission_name):
        api_url = self.setter_host + "/rest/api/latest/projects/{0}/permissions/{1}?name={2}&permission={3}".format(project_name, permission_type, related_name, permission_name)
        logger.info("Setting project '{0}' permissions belonging to the '{1}' named project for '{2}' named user/groups.".format(permission_type, project_name, related_name))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.setter_credentials, method="PUT")
            if status_code == 204:
                logger.info("'{0}' permission was granted.".format(permission_name))
                return
            elif status_code == 404:
                logger.warning("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
                return
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when setting project permission. Exception: '{0}'.".format(str(e)))

    def __set_repo_permission(self, project_name, repo_name, permission_type, related_name, permission_name):
        api_url = self.setter_host + "/rest/api/latest/projects/{0}/repos/{1}/permissions/{2}?name={3}&permission={4}".format(project_name, repo_name, permission_type, related_name, permission_name)
        logger.info("Setting repo '{0}' permissions belonging to the '{1}/{2}' named object for '{3}' named user/groups.".format(permission_type, project_name, repo_name, related_name))
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.setter_credentials, method="PUT")
            if status_code == 204:
                logger.info("'{0}' permission was granted.".format(permission_name))
                return
            elif status_code == 404:
                logger.warning("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
                return
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when setting project permission. Exception: '{0}'.".format(str(e)))

    def __import_external_repo(self, project_name, repo_name, clone_url):
        api_url = self.setter_host + "/rest/importer/latest/projects/{0}/import/repos".format(project_name)
        api_payload = json_dumper({
            "source": {
                "url": clone_url,
                "type": "GIT",
                "error": None,
                "name": repo_name
            },
            "externalRepositories": [
                {
                    "cloneUrl": clone_url,
                    "name": repo_name,
                    "description": "Imported from '{0}'".format(clone_url),
                    "scmId": "git"
                }
            ],
            "owner": "",
            "credential": {
                "username": self.getter_credentials[0],
                "password": self.getter_credentials[1],
                "error": None
            }
        })
        logger.info("Cloning repo from '{0}'.".format(clone_url))

        def wait_for_import(job_id, base_uri, credentials):
            while 1:
                control_uri = base_uri + "/rest/importer/latest/projects/{0}/import/job/{1}".format(project_name, job_id)
                c_status_code, c_response, _ = self.requester(control_uri, auth=credentials, r_type="json")
                if c_status_code == 200:
                    if c_response["tasks"][0]["state"] == "SUCCESS":
                        logger.info("Repo was cloned successfully.")
                        return
                    elif c_response["tasks"][0]["state"] == "FAILED" and c_response["tasks"][0]["failureType"] == "CREATE_CONSTRAINT_UNIQUE_REPOSITORY":
                        logger.info("Repo already imported. Skipping.")
                        return
                sleep(1)

        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.setter_credentials, r_type="json", method="POST", data=api_payload)
            if status_code == 200:
                wait_for_import(job_id=response["jobId"], base_uri=self.setter_host, credentials=self.setter_credentials)
                return
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            exit_script(-1)
        except Exception as e:
            logger.error("Got an exception when importing repo. Exception: '{0}'.".format(str(e)))

    def __set_branch_restriction(self, project_name, repo_name, payload):
        api_url = self.setter_host + "/rest/branch-permissions/latest/projects/{0}/repos/{1}/restrictions".format(project_name, repo_name)
        api_payload = json_dumper(payload)
        logger.info("Doing branch restrictions.")
        try:
            status_code, response, _ = self.requester(url=api_url, auth=self.setter_credentials, method="POST", data=api_payload)
            if status_code == 200:
                logger.info("Restrictions was set.")
                return
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when setting project permission. Exception: '{0}'.".format(str(e)))

    def __create_hook(self, project_name, repo_name, hook_key):
        api_url = self.setter_host + "/rest/api/latest/projects/{0}/repos/{1}/settings/hooks/{2}/enabled".format(project_name, repo_name, hook_key)
        api_payload = None
        try:
            _, is_enabled, is_configured = self.__get_hook_details(project_name=project_name, repo_name=repo_name, hook_key=hook_key)
            if not is_enabled:
                return
            logger.info("Creating '{0}' named hook for '{1}/{2}' named object.".format(hook_key, project_name, repo_name))
            if is_configured:
                payload_draft = self.__get_hook_settings(project_name=project_name, repo_name=repo_name, hook_key=hook_key)
                if hook_key in ["com.kylenicholls.stash.parameterized-builds:parameterized-build-hook"]:
                    extra_parameters = dict()
                    for k, v in payload_draft.iteritems():
                        if k.startswith("jobName"):
                            extra_parameters[k.replace("jobName", "jenkinsServer")] = "global-settings"
                    payload_draft.update(extra_parameters)
                api_payload = json_dumper(payload_draft)
            status_code, response, _ = self.requester(url=api_url, auth=self.setter_credentials, r_type="json", method="PUT", data=api_payload)
            if status_code == 200:
                logger.info("Hook created.")
                return
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when creating hook. Exception: '{0}'.".format(str(e)))

    def __convert_to_webhook(self, project_name, repo_name, hook_key, hook_events, url_identifier):
        api_url = self.setter_host + "/rest/api/latest/projects/{0}/repos/{1}/webhooks".format(project_name, repo_name)
        logger.info("Converting hook to webhook for '{0}' on '{1}/{2}' object.".format(hook_key, project_name, repo_name))
        try:
            hook_name, is_enabled, _ = self.__get_hook_details(project_name=project_name, repo_name=repo_name, hook_key=hook_key)
            if not is_enabled:
                return
            hook_settings = self.__get_hook_settings(project_name=project_name, repo_name=repo_name, hook_key=hook_key)
            api_payload = json_dumper({
                "active": True,
                "events": hook_events,
                "configuration": {},
                "name": hook_name,
                "url": hook_settings[url_identifier]
            })
            status_code, response, _ = self.requester(url=api_url, auth=self.setter_credentials, method="POST", data=api_payload)
            if status_code == 201:
                logger.info("Hook converted.")
                return
            logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
        except Exception as e:
            logger.error("Got an exception when converting hook. Exception: '{0}'.".format(str(e)))

    def __set_pull_requests_config(self, project_name, repo_name):
        api_url = self.setter_host + "/rest/api/latest/projects/{0}/repos/{1}/settings/hooks/{2}/enabled"
        endpoint_map = {
            "requiredApprovers": "com.atlassian.bitbucket.server.bitbucket-bundled-hooks:requiredApproversMergeHook",
            "requiredAllTasksComplete": "com.atlassian.bitbucket.server.bitbucket-bundled-hooks:incomplete-tasks-merge-check",
            "requiredSuccessfulBuilds": "com.atlassian.bitbucket.server.bitbucket-build:requiredBuildsMergeCheck"
        }
        try:
            pr_settings = self.__get_pull_requests_config(project_name=project_name, repo_name=repo_name)
            pr_done = False
            for k, v in pr_settings.iteritems():
                if isinstance(v, int) and v > 0:
                    logger.info("Setting '{0}' named pull-requests config.".format(k))
                    api_payload = json_dumper({
                        "requiredCount": str(v)
                    })
                    status_code, response, _ = self.requester(url=api_url.format(project_name, repo_name, endpoint_map[k]), auth=self.setter_credentials, method="PUT", data=api_payload)
                    if status_code == 200:
                        pr_done = True
                        logger.info("Done.")
                        continue
                    logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
                elif isinstance(v, bool) and v:
                    logger.info("Setting '{0}' named pull-requests config.".format(k))
                    status_code, response, _ = self.requester(url=api_url.format(project_name, repo_name, endpoint_map[k]), auth=self.setter_credentials, method="PUT")
                    if status_code == 200:
                        pr_done = True
                        logger.info("Done.")
                        continue
                    logger.error("The server response is not in valid response codes. Response Code: '{0}', Error: '{1}'.".format(status_code, response))
            if not pr_done:
                logger.info("There is no pull-requests config. Skipping.")
        except Exception as e:
            logger.error("Got an exception when setting pull-requests config. Exception: '{0}'.".format(str(e)))


def arg_parser():
    parser = argparse.ArgumentParser(description="Bitbucket Project Migration")
    parser.add_argument("--config-file", dest="config_file", action="store", required=True, help="Config (json formatted) file for necessary information.")
    return parser.parse_args()


if __name__ == "__main__":
    parsed_args = arg_parser()
    migrate_admin = BitBucket(config_file=parsed_args.config_file)
    migrate_admin.start_migration()
